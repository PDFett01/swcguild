function validateForm() {
	var name = document.forms["contactForm"]["name"].value;
	var email = document.forms["contactForm"]["emailAddress"].value;
	var phone = document.forms["contactForm"]["phone"].value;
	var get = document.getElementById("inquiry");
	var reason = get.options[get.selectedIndex].text;
	var additionalInformation = document.contactForm.additionalInformation.value;
	var monday = document.getElementById("monday").checked;
	var tuesday = document.getElementById("tuesday").checked;
	var wednesday = document.getElementById("wednesday").checked;
	var thursday = document.getElementById("thursday").checked;
	var friday = document.getElementById("friday").checked;

	if (name == null || name == "") {
		alert("Please enter your name");
		return false;
}

	if (email == null || email =="") {
		if (phone == null || phone =="") {
			alert("Please enter either email address or phone number");
			return false;
		}
	}		

	if (phone == null || phone =="") {
		if (email == null || email =="") {
			alert("Please enter either email address or phone number");
			return false;
		}
	}		

	if (reason=="Other") {
		if (additionalInformation == null || additionalInformation == "") {
			alert("Please provide additional information");
			return false;
		}
	}


	if (monday == false && tuesday == false && wednesday == false && thursday == false && friday == false) {
		alert("What day would be the best to contact you?");
		return false;
	}

}