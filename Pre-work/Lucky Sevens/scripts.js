

window.onload = function () {
	document.getElementById("buttonPress").addEventListener("click", rollDice); hideTable(); 
}


function hideTable () {
	var table = document.getElementById("table");
	table.style.display = "none";
}

function showTable (){
	table.style.display ="block";
}



function rollDice () {

	var startingBet = document.getElementById("startingBet").value;
	var bet = startingBet;
	var max = bet;
	var totalRollsNum = 0;
	var maxRollNum = 0;
	var startBet = document.getElementById("startBet");
	var totalRolls = document.getElementById("totalRolls");
	var rollsAtHighest = document.getElementById("rollsAtHighest");
	var highRolls = document.getElementById("highRolls");
	var playbutton = document.getElementById("buttonPress");

	while (bet > 0) {
		d1 = Math.floor(Math.random() * 6) + 1;
		d2 = Math.floor(Math.random() * 6) + 1;
		diceTotal = d1 + d2;
		totalRollsNum++;
		if (diceTotal == 7) {
			bet += 4;
			if (bet > max) {
				max = bet;
				maxRollNum = totalRollsNum;
			}
		} else {
			bet--;	
		}
	}

	startBet.innerHTML = "$" + startingBet;
	totalRolls.innerHTML = totalRollsNum;
	rollsAtHighest.innerHTML = "$" + max;
	highRolls.innerHTML = maxRollNum;
	playbutton.innerHTML = "Play Again";
	showTable();

}